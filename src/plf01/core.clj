(ns plf01.core)

(defn función-<-1
  [a]
  (< a))

(defn función-<-2
  [a b]
  (< a b))

(defn función-<-3
  [a b c]
  (< a b c))

(función-<-1 3)
(función-<-2 2 4)
(función-<-3 2 4 5)

(defn función-<=-1
 [a]
  (<= a))

(defn función-<=-2
  [a b]
  (<= a b))

(defn función-<=-3
  [a b c]
  (<= a b c))

(función-<=-1 3)
(función-<=-2 4 5)
(función-<=-3 4 5 6)

(defn función-==-1
  [x]
  (== x))

(defn función-==-2
  [x y]
  (== x y))

(defn función-==-3
  [x y z]
  (== x y z))

(función-==-1 1)
(función-==-2 1 2)
(función-==-3 1 1 1)

(defn función->-1
  [x]
  (> x))

(defn función->-2
  [x y]
  (> x y))

(defn función->-3
  [x y z]
  (> x y z))

(función->-1 2)
(función->-2 2 1)
(función->-3 2 1 3)

(defn función->=-1
  [x]
  (>= x))

(defn función->=-2
  [x y]
  (>= x y))

(defn función->=-3
  [x y z]
  (>= x y z))

(función->=-1 1)
(función->=-2 2 4)
(función->=-3 2 4 5)


(defn función-assoc-1
  [a b c]
  (assoc a b c))

(defn función-assoc-2
  [a b c]
  (assoc a b c))

(defn función-assoc-3
  [a b c]
  (assoc a b c))

(función-assoc-1  {} :nombre "Manuel")
(función-assoc-2 {:nombre "manuel"} :nombre "Victor")
(función-assoc-3 [1 2 3] 0 12)

(defn función-assoc-in-1
  [a b c]
  (assoc-in a b c))

(defn función-assoc-in-2
  [a b c]
  (assoc-in a b c))

(defn función-assoc-in-3
  [a b c]
  (assoc-in a b c))

(función-assoc-in-1 {} [:nombre "Manuel" :apellido] "Salinas")
(función-assoc-in-2 {} [] {:apellido "Salinas"})
(función-assoc-in-3 {:nombres {:pnombre "Victor" :snombre "Manuel"}} [:persona :apellido] "Salinas")

(defn función-concat-1
  [a b]
  (concat a b))

(defn función-concat-2
  [a b c]
  (concat a b c))

(defn función-concat-3
  [a b]
  (concat a b))

(función-concat-1 [1 2 3 4] [5 6 7 8])
(función-concat-2 [1 2 3 4] [5 6 7 8] {9 10})
(función-concat-3 "Manuel" "Salinas")

(defn función-conj-1
  [a b]
  (conj a b))

(defn función-conj-2
  [a b c]
  (conj a b c))

(defn función-conj-3
  [a b c d]
  (conj a b c d))

(función-conj-1 [1 2 3 4] 5)
(función-conj-2 ["Victor" "Manuel"] "Vasquez" "Salinas")
(función-conj-3 [1 2 3] [4 5 6] '(7 8) '(9 10))

(defn función-contains?-1
  [a b]
  (contains? a b))

(defn función-contains?-2
  [a b]
  (contains? a b))

(defn función-contains?-3
  [a b]
  (contains? a b))

(función-contains?-1 {:nombre "Manuel" :apellido "Salinas"} :nombre)
(función-contains?-2 {"nombre" "Manuel" "apellido" "Salinas"} "nombre")
(función-contains?-3 #{"Manuel" "Salinas"} "Salinas")

(defn función-count-1
  [a]
  (count a))

(defn función-count-2
  [x]
  (count x))

(defn función-count-3
  [y]
  (count y))

(función-count-1 "Manuel")
(función-count-2 ["Manuel" "Salinas"])
(función-count-3 {:nombre "Manuel" :apellido "Salinas"})

(defn función-disj-1
  [a]
  (disj a))

(defn función-disj-2
  [a b]
  (disj a b))

(defn función-disj-3
  [a b c]
  (disj a b c))

(función-disj-1 #{1 2 3 4})
(función-disj-2 #{1 2 3 4} 2)
(función-disj-3 #{1 2 3 4} 1 4)

(defn función-dissoc-1
  [a]
  (dissoc a))

(defn función-dissoc-2
  [a b]
  (dissoc a b))

(defn función-dissoc-3
  [a b c]
  (dissoc a b c))

(función-dissoc-1 {:nombre "Manuel" :apellido "Salinas"})
(función-dissoc-2 {:nombre "Manuel" :apellido "Salinas"} :nombre)
(función-dissoc-3 {:nombre "Manuel" :apellido "Salinas"} :nombre :apellido)

(defn función-distinct-1
  [a]
  (distinct a))

(defn función-distinct-2
  [x]
  (distinct x))

(defn función-distinct-3
  [y]
  (distinct y))

(función-distinct-1 [1 1 2 3 4 5 5 5 6])
(función-distinct-2 ["Manuel" "Manuel" "Salinas"])
(función-distinct-3 '(1 2 3 44 5 5 5 6 7 7 7))

(defn función-distinct?-1
  [a b]
  (distinct? a b))

(defn función-distinct?-2
  [a b c]
  (distinct? a b c))

(defn función-distinct?-3
  [a b c d]
  (distinct? a b c d))

(función-distinct?-1 [1 2 3] [1 2 4])
(función-distinct?-2 1 2 3)
(función-distinct?-3 1 2 "Manuel" "Manuel")

(defn función-drop-last-1
  [a]
  (drop-last a))

(defn función-drop-last-2
  [a b]
  (drop-last a b))

(defn función-drop-last-3
  [a b]
  (drop-last a b))

(función-drop-last-1 [1 2 3 4])
(función-drop-last-2 2 [1 2 3 4])
(función-drop-last-3 2 {:nombre "Manuel" :apellidop "Salinas" :apellidom "Vasquez"})

(defn función-empty-1
  [a]
  (empty a))

(defn función-empty-2
  [x]
  (empty x))

(defn función-empty-3
  [y]
  (empty y))

(función-empty-1 [1 2 3 4 5])
(función-empty-2 {:nombre "Manuel" :apellido "Salinas"})
(función-empty-3 [["Manuel"] ["Salinas"] 1 2 4])

(defn función-empty?-1
  [a]
  (empty? a))

(defn función-empty?-2
  [x]
  (empty? x))

(defn función-empty?-3
  [y]
  (empty? y))

(función-empty?-1 [])
(función-empty?-2 "")
(función-empty?-3 {:nombre "Manuel"})

(defn función-even?-1
  [a]
  (even? a))

(defn función-even?-2
  [a b]
  (even? (+ a b)))

(defn función-even?-3
  [a b]
  (even? (/ a b)))

(función-even?-1 -4)
(función-even?-2 -4 1)
(función-even?-3 9 3)

(defn función-false?-1
  [a]
  (false? a))

(defn función-false?-2
  [b]
  (false? b))

(defn función-false?-3
  [a b]
  (false? (> a b)))

(función-false?-1 false)
(función-false?-1 true)
(función-false?-3 2 3)

(defn función-find-1
  [a b]
  (find a b))

(defn función-find-2
  [x y]
  (find x y))

(defn función-find-3
  [z w]
  (find z w))

(función-find-1 {:nombre "Manuel" :apellido "Salinas"} :nombre)
(función-find-2 {"nombre" "Manuel" "apellido" "Salinas"} "nombre")
(función-find-3 {"nombre" "Manuel" "apellido" "Salinas"} 0)

(defn función-first-1
  [a]
  (first a))

(defn función-first-2
  [x]
  (first x))

(defn función-first-3
  [y]
  (first y))

(función-first-1 [1 2 3 4 5])
(función-first-2 '(1 2 3 4 5))
(función-first-3 "Manuel")

(defn función-flatten-1
  [a]
  (flatten a))

(defn función-flatten-2
  [x]
  (flatten x))

(defn función-flatten-3
  [y]
  (flatten y))

(función-flatten-1 [[1 2 3 '("manuel")]["Salinas"]])
(función-flatten-2 '(12 3 4 5 5 (1 2 3 4)))
(función-flatten-3 [1 34 [["Manuel" ["Salinas" 2 [1 2 3 4]]] 2]])

(defn función-frequencies-1
  [a]
  (frequencies a))

(defn función-frequencies-2
  [x]
  (frequencies x))

(defn función-frequencies-3
  [x]
  (frequencies x))

(función-frequencies-1 "Mannuel")
(función-frequencies-2 [1 2 3 3 4 4 55 6 6 9])
(función-frequencies-3 ["Manuel" "Manuel" "Salinas" "Vasquez"])

(defn función-get-1
  [a b]
  (get a b))

(defn función-get-2
  [x y]
  (get x y))

(defn función-get-3
  [z w]
  (get z w))

(función-get-1 [1 2 3 4 5 6 7] 3)
(función-get-2 "Manuel" 3)
(función-get-3 {:nombre "Manuel" :apellido "Salinas"} :nombre)

(def tab {:nombre "Manuel" :apellido "Salinas" :datos {:nc "C16920405" :c "ISC" :a {:sem 9}}})
(defn función-get-in-1
  [b]
  (get-in tab b))

(defn función-get-in-2
  [x]
  (get-in tab x))

(defn función-get-in-3
  [y]
  (get-in tab y))

(función-get-in-1 [:nombre])
(función-get-in-2 [:datos :nc])
(función-get-in-2 [:datos :a :sem])


(defn función-into-1
  [a b]
  (into a b))

(defn función-into-2
  [x y]
  (into x y))

(defn función-into-3
  [z w]
  (into z w))

(función-into-1 [] "Manuel")
(función-into-2 [] {:a \a :b \b})
(función-into-3 '() {:a \a :b \b})

(defn función-key-1
  [a]
  (key a))

(función-key-1 {:nombre "Manuell"}) 

(defn función-keys-1
  [a]
  (keys a))

(defn función-keys-2
  [b]
  (keys b))

(defn función-keys-3
  [x]
  (keys x))

(función-keys-1 {:nombre "Manuel" :apellido "Salinas"})
(función-keys-2 {"nombre ""Manuel" "apellido" "Salinas"})
(función-keys-3 {1 2 3 4})

(defn función-max-1
  [a b]
  (max a b))

(defn función-max-2
  [a b c]
  (max a b c))

(defn función-max-3
  [a b c d]
  (max a b c d))

(función-max-1 1 5)
(función-max-2 34 5 67)
(función-max-3 34 5 67 124)

(defn función-merge-1
  [a b] 
  (merge a b))

(defn función-merge-2
  [a b c]
  (merge a b c))

(defn función-merge-3
  [a b c d]
  (merge a b c d))

(función-merge-1 {:a \a :b \b} {1 2 3 4})
(función-merge-2 {:a \a :b \b} {1 2 3 4} {"nombre" "Manuel"})
(función-merge-3 {:a \a :b \b} {1 2 3 4} {"nombre" "Manuel"} {\a 1})

(defn función-min-1
  [a b]
  (min a b))

(defn función-min-2
  [a b c]
  (min a b c))

(defn función-min-3
  [a b c d]
  (min a b c d))

(función-min-1 45 6)
(función-min-2 45 6 -2)
(función-min-3 45 6 -2 -31453)

(defn función-neg?-1
  [a]
  (neg? a))

(defn función-neg?-2
  [a b]
  (neg? (- a b)))

(defn función-neg?-3
  [a b]
  (neg? (* a b)))

(función-neg?-1 -5)
(función-neg?-2 5 465)
(función-neg?-3 -1 2)

(defn función-nil?-1
  [a]
  (nil? a))

(defn función-nil?-2
  [x]
  (nil? (first x)))

(defn función-nil?-3
  [x y]
  (nil? (find x y)))

(función-nil?-1 nil)
(función-nil?-2 [])
(función-nil?-3 {:a 1 :b 2} :c)

(defn función-not-empty-1
  [a]
  (not-empty a))

(defn función-not-empty-2
  [a b]
  (not-empty (concat a b)))

(defn función-not-empty-3
  [x]
  (not-empty x))

(función-not-empty-1 [1 2 3 4])
(función-not-empty-2 [1 2 3 4] '(1 2 3 4))
(función-not-empty-3 "Manuel")

(defn función-nth-1
  [a b]
  (nth a b))

(defn función-nth-2
  [a b c ]
  (nth a b c))

(defn función-nth-3
  [x y]
  (nth x y))

(función-nth-1 [1 2 3 4 5] 0)
(función-nth-2 [\a \b \c \d] 8 "No encontrado")
(función-nth-3 '("Manuel" "Salinas" "ISC") 2)

(defn función-odd?-1
 [a]
  (odd? a))

(defn función-odd?-2
  [a b]
  (odd? (+ a b)))

(defn función-odd?-3
  [x y]
  (odd? (- x y)))

(función-odd?-1 45)
(función-odd?-2 67 84)
(función-odd?-3 5 76)

(defn función-partition-1
  [a b]
  (partition a b))

(defn función-partition-2
  [a b c]
  (partition a b c))

(defn función-partition-3
  [a b c d]
  (partition a b c d))

(función-partition-1 3 [1 2 3 4 56 8])
(función-partition-2 2 1 [1 2 3 4 56 8 35 77])
(función-partition-3 2 1 ["numeros"] [1 2 3 4 56 8 35 77])

(defn función-partition-all-1
  [a b]
  (partition-all a b))

(defn función-partition-all-2
  [a b c]
  (partition-all a b c))

(defn función-partition-all-3
  [x y]
  (partition-all x y))

(función-partition-all-1 2 ["Victor" "Manuel" "Salinas"])
(función-partition-all-2 2 1 ["Victor" "Manuel" "Salinas" "ISC" "ITO"])
(función-partition-all-3 5 '(12 3 4))

(defn función-peek-1
  [a]
  (peek a))

(defn función-peek-2
  [x]
  (peek x))

(defn función-peek-3
  [y]
  (peek y))

(función-peek-1 [1 2 3 4 5 6]) 
(función-peek-2 '(\a \b \c \d))
(función-peek-3 ["Manuel" "Salinas" "ISC"])

(defn función-pop-1
  [a]
  (pop a))

(defn función-pop-2
  [x]
  (pop x))

(defn función-pop-3
  [y]
  (pop y))

(función-pop-1 [1 2 3 4 5 6])
(función-pop-2 '("Manuel" "Salinas" "ISC" "ITO"))
(función-pop-3 [\a \b \c \d ])

(defn función-pos?-1
  [a]
  (pos? a))

(defn función-pos?-2
  [a b]
  (pos? (+ a b)))

(defn función-pos?-3
  [a b]
  (pos? (- a b)))

(función-pos?-1 -4564)
(función-pos?-2 -123 4234)
(función-pos?-3  3455 7567)

(defn función-quot-1
  [a b]
  (quot a b))

(defn función-quot-2
  [x y]
  (quot x y))

(defn función-quot-3
  [z w]
  (quot z w))

(función-quot-1 132 543)
(función-quot-1 -34 -26)
(función-quot-3 5 65)

(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a b]
  (range a b))

(defn función-range-3
  [a b c]
  (range a b c))

(función-range-1 9)
(función-range-2 9 45)
(función-range-3 9 45 5)

(defn función-rem-1
  [a b]
  (rem a b))

(defn función-rem-2
  [x y]
  (rem x y))

(defn función-rem-3
  [z w]
  (rem z w))

(función-rem-1 435 -45)
(función-rem-2 -435.9 -45)
(función-rem-3 0.43 0.24)

(defn función-repeat-1
  [a b]
  (repeat a b))

(defn función-repeat-2
  [x y]
  (repeat x y))

(defn función-repeat-3
  [z w]
  (repeat z w))

(función-repeat-1 5 "X")
(función-repeat-2 10 9)
(función-repeat-3 10 [1 2 3 4])

(defn función-replace-1
  [a]
  (replace a))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [x y]
  (replace x y))

(función-replace-1 "Manuel")
(función-replace-2 2 10)
(función-replace-3 [1 2 3 4] '(1 2 34))

(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [b]
  (rest b))

(defn función-rest-3
  [x]
  (rest x))

(función-rest-1 [1 2 3 4 5])
(función-rest-2 ["Manuel" "Salinas"])
(función-rest-3 '(nil))

(defn función-select-key-1
  [a b]
  (select-keys a b))

(defn función-select-key-2
  [x y]
  (select-keys x y))

(defn función-select-key-3
  [z w]
  (select-keys z w))

(función-select-key-1 {:a \a :b \b} [:b])
(función-select-key-2 {1 2 3 4 5 6} [5])
(función-select-key-3 {:a "m" :c "d"}[:a])

(defn función-shuffle-1
  [a]
  (shuffle a))

(defn función-shuffle-2
  [b]
  (shuffle b))

(defn función-shuffle-3
  [x]
  (shuffle x))

(función-shuffle-1 '(1 2 3 4 5))
(función-shuffle-2  #{1 2 3 4})
(función-shuffle-3  [1 2 3 4 5])

(defn función-sort-1
  [a]
  (sort a))

(defn función-sort-2
  [x]
  (sort x))

(defn función-sort-3
  [y]
  (sort y))

(función-sort-1 [1234 231 3 6 3])
(función-sort-2 '("Manuel" "Vasquez" "Salinas"))
(función-sort-3 [\a \z \b \t])

(defn función-spli-at-1
  [a b]
  (split-at a b))

(defn función-spli-at-2
  [x y]
  (split-at x y))

(defn función-spli-at-3
  [z w]
  (split-at z w))

(función-spli-at-1 2 [1 12 657 3 323])
(función-spli-at-2 3 '("Manuel" "Vasquez" "ISC"))
(función-spli-at-3 0 [\A \B \C \D])

(defn función-str-1
  [a] 
  (str a))

(defn función-str-2
  [a b]
  (str a b))

(defn función-str-3
  [a b c]
  (str a b c))

(función-str-1 [\m \a \n \u \e \l])
(función-str-2 3 4)
(función-str-3 \m \a \n)

(defn función-subs-1
  [a b]
  (subs a b))

(defn función-subs-2
  [a b c]
  (subs a b c))

(defn función-subs-3
  [x y]
  (subs x y))

(función-subs-1 "Manuel" 3)
(función-subs-2 "Salians" 2 3)
(función-subs-3 "ITO" 0)

(defn función-subvec-1
  [a b]
  (subvec a b))

(defn función-subvec-2
  [a b c]
  (subvec a b c))

(defn función-subvec-3
  [x y]
  (subvec x y))

(función-subvec-1 [1 2 3 4 5] 3)
(función-subvec-2 ["A" "B" "C" "D" "E"] 2 4)
(función-subvec-3 [] 0)

(defn función-take-1
  [a]
  (take a))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [x y]
  (take x y))

(función-take-1 [1 2 3 4 5])
(función-take-2 3 [1 2 23 4])
(función-take-3 2 "Manuel")

(defn función-true-1
  [a]
  (true? a))

(defn función-true-2
  [x y]
  (true? (< x y)))

(defn función-true-3
  [a b]
  (true? (> a b)))

(función-true-1 true)
(función-true-2 3 4)
(función-true-3 -1 5)

(defn función-val-1
  [a]
  (map val a))

(función-val-1 {1 2 3 4 5 6 7 8})

(defn función-vals-1
  [a]
  (vals a))

(defn función-vals-2
  [b]
  (vals b))

(defn función-vals-3
  [x]
  (vals x))

(función-vals-1 {:a "a" :b "b"})
(función-vals-2 {"a" "a" "b" "b"})
(función-vals-3 {\a 1 \b 2})

(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a b]
  (zero? (/ a b)))

(defn función-zero?-3
  [a b]
  (zero? (- a b)))

(función-zero?-1 0)
(función-zero?-2 1 2)
(función-zero?-3 5 5)

(defn función-zipmap-1
  [a b]
  (zipmap a b))

(defn función-zipmap-2
  [x y]
  (zipmap x y))

(defn función-zipmap-3
  [z w]
  (zipmap z w))

(función-zipmap-1 [1 2 3 4] [\a \b \c \d])
(función-zipmap-2 '(1 2 3 4) ["a" "b" "c" "d"])
(función-zipmap-3 '({1 2 3 4} 1) '("Manuel" \a))